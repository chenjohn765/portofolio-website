import { Dispatch, SetStateAction, createContext } from "react";

export const BlurContentContext = createContext<
  Dispatch<SetStateAction<boolean>>
>(() => {
  return null;
});
