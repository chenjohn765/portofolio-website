import ZoomOutViewOnceDiv from "../clientComponents/zoomOutViewDiv";
import { JLogoAnimated } from "../svg";

export const SplashScreen = () => (
  <ZoomOutViewOnceDiv
    options={{ delay: "delay-[3000ms]", duration: "duration-[300ms]" }}
  >
    <div className="flex w-full h-screen items-center justify-content">
      <div className="w-24 h-24 text-green">
        <JLogoAnimated />
      </div>
    </div>
  </ZoomOutViewOnceDiv>
);
