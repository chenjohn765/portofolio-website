"use client";
import NavBar from "./navBar/navBar";

import SideBar from "./sideBar/sideBar";
import Intro from "./intro/intro";
import AboutMe from "./content/aboutMe/aboutMe";
import Experience from "./content/experience/experience";
import Projects from "./content/projects/projects";
import Contact from "./content/contact/contact";
import FadeInViewOnceDiv from "./clientComponents/fadeInViewOnceDiv";
import { zIndexes } from "./zIndex";
import { useEffect, useState } from "react";
import { BlurContentContext } from "./contexts";
import { SplashScreen } from "./splashScreen/splashScreen";

const Content = () => (
  <>
    <Intro />
    <AboutMe />
    <Experience />
    <Projects />
    <Contact />
  </>
);

export default function Home() {
  const [shouldBlurContent, setShouldBlurContent] = useState(false);
  const [mainPageShouldAppear, setMainPageShouldAppear] = useState(false);

  useEffect(() => {
    // The Splash screen takes 3s to complete. Display main content after that;
    setTimeout(() => setMainPageShouldAppear(true), 3000);
  }, []);

  return (
    <BlurContentContext.Provider value={setShouldBlurContent}>
      <main className="flex min-h-screen flex-col items-center justify-between">
        <SplashScreen />
        {mainPageShouldAppear && (
          <div className="grid grid-cols-12 w-full">
            <div
              className={`
                col-span-12
                pl-6 pr-6 ipad:pl-12 ipad:pr-12
                sticky top-0 ${zIndexes.navBar} opacity-90`}
            >
              {/* Be careful, when you change the duration here
          You also need to change the delay below for Content.
          Content must appear AFTER NavBar has displayed*/}
              <FadeInViewOnceDiv options={{ duration: `duration-[1000ms]` }}>
                <NavBar />
              </FadeInViewOnceDiv>
            </div>
            <div className="col-span-1 ipad:col-span-2">
              <SideBar />
            </div>
            <div
              className={`
              col-span-10 ipad:col-span-8 mt-24
              ${shouldBlurContent ? "blur" : ""}
              `}
            >
              {/* We fade in only once the nav bar has appeared */}
              <FadeInViewOnceDiv options={{ delay: "delay-[1000ms]" }}>
                <Content />
              </FadeInViewOnceDiv>
            </div>
            {/* side */}
            <div className="col-span-1 ipad:col-span-2"></div>
          </div>
        )}
      </main>
    </BlurContentContext.Provider>
  );
}
