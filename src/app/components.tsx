import Image from "next/image";
import { ReactNode } from "react";
import { space_mono } from "./fonts";

export const GreenButton = ({ children }: { children: ReactNode }) => (
  <button
    className={`
    border-[1px] border-green rounded p-4 green
    shadow-hover
    ${space_mono.className}
    `}
  >
    {children}
  </button>
);

const defaultTitlecss = `font-bold text-gray-700`;
export const MaxXLTitlecss = `${defaultTitlecss} text-[60px]`;
export const XXLTitlecss = `${defaultTitlecss} text-[40px]`;
export const XLTitlecss = `${defaultTitlecss} text-2xl`;
export const SMTitlecss = `${defaultTitlecss} text-xl`;

export const MaxXLTitle = ({ children }: { children: ReactNode }) => (
  <h1 className={MaxXLTitlecss}>{children}</h1>
);
export const XXLTitle = ({ children }: { children: ReactNode }) => (
  <h1 className={XXLTitlecss}>{children}</h1>
);
export const XLTitle = ({ children }: { children: ReactNode }) => (
  <h1 className={XLTitlecss}>{children}</h1>
);
export const SMTitle = ({ children }: { children: ReactNode }) => (
  <h2 className={SMTitlecss}>{children}</h2>
);

export const SectionTitle = ({
  index,
  title,
  id,
}: {
  index: string;
  title: string;
  id: string;
}) => (
  <div className="flex items-center mb-8" id={id}>
    <span className={`green text-[20px] mr-1 ${space_mono.className}`}>
      {index}.{" "}
    </span>
    <XLTitle>{title}</XLTitle>
    <div className="h-[1px] w-[300px] bg-[#233554] ml-4" />
  </div>
);

// Images
interface IImage {
  src: string;
  width: number;
  height: number;
  alt: string;
}
export const GreenImage = ({ src, width, height, alt }: IImage) => (
  <div className="bg-green rounded">
    <Image
      className="green-image transition-[var(--transition)]"
      src={src}
      width={width}
      height={height}
      alt={alt}
    />
  </div>
);

export const GreenImageWithBorder = ({ src, width, height, alt }: IImage) => (
  <div
    // relative is used to allow the position:absolute of the after element
    // So that this after element is position starting from this relative parent
    // The transition -4px of the parent will affect the after element.
    // We put transition +8px on the after element to compensate
    // The Bezier transition will make sure that those two combined will be smooth
    // The h-fit is so that this div class doesn't strech in height, as the grid
    // can become bigger.
    className="
  h-fit
  rounded
  relative
  transition-[var(--transition)] hover:-translate-x-[4px] hover:-translate-y-[4px]
  bg-green
  after:absolute after:inline-block
  after:h-full after:w-full
  after:top-3 after:left-3 after:-z-[1]
  after:border-green after:border-[1px]
  after:rounded
  after:hover:translate-x-[8px] after:hover:translate-y-[8px] after:transition-[var(--transition)]
  "
  >
    <Image
      className="green-image transition-[var(--transition)]"
      src={src}
      width={width}
      height={height}
      alt={alt}
    />
  </div>
);

export const HidePhone = ({
  children,
  alt,
  className,
}: {
  children: ReactNode;
  alt?: ReactNode;
  className?: string;
}) => (
  <>
    <div className={"hidden ipad:block ipad:visible" + " " + className}>
      {children}
    </div>
    <div className={"visible ipad:hidden" + " " + className}>{alt}</div>
  </>
);
