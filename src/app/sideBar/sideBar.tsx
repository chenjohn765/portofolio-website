import Link from "next/link";
import { EmailLogo, GithubLogo, LinkedInLogo, TwitterLogo } from "../svg";

export const SocialMedia = () => (
  <ol className="flex items-center">
    <li className="w-[30px] mr-4 hover:green hover:-translate-y-[4px] transition-[var(--transition)]">
      <Link href="#">
        <GithubLogo />
      </Link>
    </li>
    <li className="w-[30px] mr-4 hover:green hover:-translate-y-[4px] transition-[var(--transition)]">
      <Link href="#">
        <TwitterLogo />
      </Link>
    </li>
    <li className="w-[30px] mr-4 hover:green hover:-translate-y-[4px] transition-[var(--transition)]">
      <Link
        target="_blank"
        href="https://fr.linkedin.com/in/johnny-chen-a11233161"
      >
        <LinkedInLogo />
      </Link>
    </li>
    <li className="w-[30px] mr-4 hover:green hover:-translate-y-[4px] transition-[var(--transition)]">
      <Link target="_blank" href="mailto:chenJohn765@gmail.com">
        <EmailLogo />
      </Link>
    </li>
  </ol>
);

const SideBar = () => (
  <div className="w-[10%]">
    <SocialMedia />
  </div>
);
export default SideBar;
