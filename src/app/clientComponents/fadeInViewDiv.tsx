"use client";
import { useFadeInView } from "@/app/hooks";
import { ReactNode } from "react";

const FadeInViewDiv = ({ children }: { children: ReactNode }) => {
  const { ref, css: fadeInCss } = useFadeInView();

  return (
    <div className={fadeInCss} ref={ref}>
      {children}
    </div>
  );
};

export default FadeInViewDiv;
