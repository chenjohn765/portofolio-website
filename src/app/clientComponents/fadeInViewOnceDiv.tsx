"use client";
import { IFadeIn, useFadeInViewOnce } from "@/app/hooks";
import { ReactNode } from "react";

const FadeInViewOnceDiv = ({
  children,
  options,
}: {
  children: ReactNode;
  options?: IFadeIn;
}) => {
  const { ref, css: fadeInCss } = useFadeInViewOnce(options);

  return (
    <div className={fadeInCss} ref={ref}>
      {children}
    </div>
  );
};

export default FadeInViewOnceDiv;
