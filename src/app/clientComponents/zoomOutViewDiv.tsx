"use client";
import { IFadeIn, useZoomOutViewOnce } from "@/app/hooks";
import { ReactNode } from "react";

const ZoomOutViewOnceDiv = ({
  children,
  options,
  className,
}: {
  children: ReactNode;
  options?: IFadeIn;
  className?: string;
}) => {
  const { ref, css: zoomOutCss } = useZoomOutViewOnce(options);

  return (
    <div className={`${zoomOutCss} ${className}`} ref={ref}>
      {children}
    </div>
  );
};

export default ZoomOutViewOnceDiv;
