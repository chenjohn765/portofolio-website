import { useEffect, useState } from "react";
import { useInView } from "react-intersection-observer";

export interface IFadeIn {
  // The values should be the exact tailwind css string
  // ex: {duration: "duration-200"}
  duration?: string;
  delay?: string;
}

export const useFadeInView = (options?: IFadeIn) => {
  const viewHook = useInView();
  const duration = options?.duration || "duration-500";
  const delay = options?.delay || "delay-200";
  const fadeInCss = `
  opacity-0
  ${duration}
  ${delay}
  transition-[opacity]
  ${viewHook.inView ? "opacity-100" : ""}
  `;
  return Object.assign(viewHook, { css: fadeInCss });
};

export const useFadeInViewOnce = (options?: IFadeIn) => {
  const viewHook = useInView();
  const [viewedOnce, setViewedOnce] = useState(false);

  const duration = options?.duration || "duration-500";
  const delay = options?.delay || "delay-200";

  useEffect(() => {
    setViewedOnce((viewedOnce) => viewedOnce || viewHook.inView);
  }, [viewHook.inView]);

  const fadeInCss = `
    opacity-0
    ${duration}
    ${delay}
    transition-[opacity]
    ${viewedOnce ? "opacity-100" : ""}
    `;
  return Object.assign(viewHook, { css: fadeInCss });
};

export const useZoomOutViewOnce = (options?: IFadeIn) => {
  const viewHook = useInView();
  const [viewedOnce, setViewedOnce] = useState(false);

  const duration = options?.duration || "duration-500";
  const delay = options?.delay || "delay-200";

  useEffect(() => {
    setViewedOnce((viewedOnce) => viewedOnce || viewHook.inView);
  }, [viewHook.inView]);

  const fadeInCss = `
    ${duration}
    ${delay}
    ease-in
    ${viewedOnce ? "scale-0 collapse" : "scale-100"}
    `;
  return Object.assign(viewHook, { css: fadeInCss });
};
