import FadeInViewOnceDiv from "./clientComponents/fadeInViewOnceDiv";
import { useFadeInViewOnce } from "./hooks";
import "./animate.css";

export const JLogo = () => (
  // <svg
  //   id="logo"
  //   xmlns="http://www.w3.org/2000/svg"
  //   role="img"
  //   viewBox="0 0 84 96"
  // >
  //   <title>Logo</title>
  //   <g transform="translate(-8.000000, -2.000000)">
  //     <g transform="translate(11.000000, 5.000000)">
  //       <polygon
  //         id="Shape"
  //         stroke="currentColor"
  //         strokeWidth="5"
  //         strokeLinecap="round"
  //         strokeLinejoin="round"
  // fill="var(--navy)"
  //         points="39 0 0 22 0 67 39 90 78 68 78 23"
  //       ></polygon>
  //       <path
  //         //   d="M50,33 L50,61 L40,61 L40,45 L25,45 L25,33 L50,33 Z"
  //         //   d="M50,50 H100 Z"
  //         d="M45.691667,45.15 C48.591667,46.1 50.691667,48.95 50.691667,52.2 C50.691667,57.95 46.691667,61 40.291667,61 L28.541667,61 L28.541667,30.3 L39.291667,30.3 C45.691667,30.3 49.691667,33.15 49.691667,38.65 C49.691667,41.95 47.941667,44.35 45.691667,45.15 Z M33.591667,43.2 L39.241667,43.2 C42.791667,43.2 44.691667,41.85 44.691667,38.95 C44.691667,36.05 42.791667,34.8 39.241667,34.8 L33.591667,34.8 L33.591667,43.2 Z M33.591667,47.5 L33.591667,56.5 L40.191667,56.5 C43.691667,56.5 45.591667,54.75 45.591667,52 C45.591667,49.2 43.691667,47.5 40.191667,47.5 L33.591667,47.5 Z"
  //         fill="currentColor"
  //       ></path>
  //     </g>
  //   </g>
  // </svg>
  <svg
    id="logo"
    xmlns="http://www.w3.org/2000/svg"
    role="img"
    viewBox="0 0 84 96"
  >
    <title>Logo</title>
    <g transform="translate(-8.000000, -2.000000)">
      <g transform="translate(11.000000, 5.000000)">
        <polygon
          id="Shape"
          stroke="currentColor"
          strokeWidth="5"
          strokeLinecap="round"
          strokeLinejoin="round"
          points="39 0 0 22 0 67 39 90 78 68 78 23"
          className="fill-navy-900"
        ></polygon>
        {/* Taken from https://youtu.be/-fIQy99I9wc?t=89 */}
        <text
          x="33"
          y="60"
          fill="currentColor"
          fontSize="45px"
          fontFamily="Consolas"
        >
          {"J"}
        </text>
      </g>
    </g>
  </svg>
);

export const JLogoAnimated = () => {
  const { ref, css: fadeInCss } = useFadeInViewOnce({
    delay: "delay-[1500ms]",
    duration: "duration-500",
  });
  return (
    <svg
      id="logo"
      xmlns="http://www.w3.org/2000/svg"
      role="img"
      viewBox="0 0 84 96"
    >
      <title>Logo</title>
      <g transform="translate(-8.000000, -2.000000)">
        <g transform="translate(11.000000, 5.000000)">
          <polygon
            id="Shape"
            stroke="currentColor"
            strokeWidth="5"
            strokeLinecap="round"
            strokeLinejoin="round"
            points="39 0 0 22 0 67 39 90 78 68 78 23"
            className="fill-navy-900 polygonStrokeAnimate"
          ></polygon>
          {/* Taken from https://youtu.be/-fIQy99I9wc?t=89 */}
          <text
            ref={ref}
            x="33"
            y="60"
            fill="currentColor"
            fontSize="45px"
            fontFamily="Consolas"
            // Text appear after 1s, the time that the polygon animates
            className={fadeInCss}
          >
            {"J"}
          </text>
        </g>
      </g>
    </svg>
  );
};

export const GithubLogo = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    role="img"
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
    // className="feather feather-github"
  >
    <title>GitHub</title>
    <path d="M9 19c-5 1.5-5-2.5-7-3m14 6v-3.87a3.37 3.37 0 0 0-.94-2.61c3.14-.35 6.44-1.54 6.44-7A5.44 5.44 0 0 0 20 4.77 5.07 5.07 0 0 0 19.91 1S18.73.65 16 2.48a13.38 13.38 0 0 0-7 0C6.27.65 5.09 1 5.09 1A5.07 5.07 0 0 0 5 4.77a5.44 5.44 0 0 0-1.5 3.78c0 5.42 3.3 6.61 6.44 7A3.37 3.37 0 0 0 9 18.13V22"></path>
  </svg>
);

export const TwitterLogo = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    role="img"
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
    // className="feather feather-twitter"
  >
    <title>Twitter</title>
    <path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path>
  </svg>
);

export const LinkedInLogo = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    role="img"
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
    // className="feather feather-linkedin"
  >
    <title>LinkedIn</title>
    <path d="M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z"></path>
    <rect x="2" y="9" width="4" height="12"></rect>
    <circle cx="4" cy="4" r="2"></circle>
  </svg>
);

export const EmailLogo = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    role="img"
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <title>Email</title>
    <path d="M5.25 6.75h13.5m-13.5 0v10.5h13.5V6.75m-13.5 0L12 12l6.75-5.25"></path>
  </svg>
);

export const ExternalLinkLogo = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    role="img"
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <title>External Link</title>
    <path d="M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6"></path>
    <polyline points="15 3 21 3 21 9"></polyline>
    <line x1="10" y1="14" x2="21" y2="3"></line>
  </svg>
);

export const InclinedHamburgerLogo = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    role="img"
    fill="none"
    viewBox="0 0 36 24"
    height={24}
    width={36}
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <rect width="36" height="1"></rect>
    <rect x="6" y="10" width="30" height="1"></rect>
    <rect x="12" y="20" width="24" height="1"></rect>
  </svg>
);

export const CrossSVG = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    role="img"
    fill="none"
    viewBox="0 0 30 30"
    width={30}
    height={30}
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <line x1="0" y1="0" x2="30" y2="30" />
    <line x1="0" y1="30" x2="30" y2="0" />
  </svg>
);
