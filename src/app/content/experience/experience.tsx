"use client";
import { useState } from "react";
import { SMTitle, SectionTitle } from "../../components";

import { Inter } from "next/font/google";
import FadeInViewOnceDiv from "@/app/clientComponents/fadeInViewOnceDiv";
import { space_mono } from "@/app/fonts";

const FeatureStoreContent = () => (
  <div>
    <SMTitle>
      Python engineer <span className="green">@ Polyconseil</span>
    </SMTitle>
    <p>June 2021 - June 2023</p>
    <br />
    <ul>
      <li className="before-list-icon">
        Build a FeatureStore for machine learning engineering: a system that
        centralizes definition of features, compute features periodically or on
        the fly, and facilitates re-usability & versioning of features.
      </li>
      <li className="before-list-icon">Python & FastAPI & SQLAlchemy</li>
      <li className="before-list-icon">Terraform</li>
      <li className="before-list-icon">AWS Glue & Lambda</li>
    </ul>
  </div>
);

const CoviflexContent = () => (
  <div>
    <SMTitle>
      Fullstack engineer <span className="green">@ Polyconseil</span>
    </SMTitle>
    <p>June 2020 - June 2021</p>
    <br />
    <ul>
      <li className="before-list-icon">
        Build Coviflex: a web application to monitor in live who is in the
        office & where, with KPI.
      </li>
      <li className="before-list-icon">NodeJS Typescript + React Typescript</li>
    </ul>
  </div>
);

const Experience = () => {
  const sections: Array<[string, () => JSX.Element]> = [
    ["FeatureStore", FeatureStoreContent],
    ["Coviflex", CoviflexContent],
    ["FeatureStore", FeatureStoreContent],
    ["Coviflex", CoviflexContent],
  ];
  const tabs = sections.map((section) => section[0]);
  const [tabIndex, setTabIndex] = useState(0);

  //   https://tailwindcss.com/docs/content-configuration#dynamic-class-names
  //   We do it like this because it's not recommmended to do dynamic classes by interpolation
  const translateY = [
    "ipad:translate-y-[calc(0*40px)]",
    "ipad:translate-y-[calc(1*40px)]",
    "ipad:translate-y-[calc(2*40px)]",
    "ipad:translate-y-[calc(3*40px)]",
    "ipad:translate-y-[calc(4*40px)]",
    "ipad:translate-y-[calc(5*40px)]",
    "ipad:translate-y-[calc(6*40px)]",
    "ipad:translate-y-[calc(7*40px)]",
  ];

  const translateX = [
    "translate-x-[calc(0*130px)]",
    "translate-x-[calc(1*130px)]",
    "translate-x-[calc(2*130px)]",
    "translate-x-[calc(3*130px)]",
    "translate-x-[calc(4*130px)]",
    "translate-x-[calc(5*130px)]",
    "translate-x-[calc(6*130px)]",
    "translate-x-[calc(7*130px)]",
  ];

  return (
    <FadeInViewOnceDiv>
      <div className="mb-48">
        <SectionTitle index={"02"} title={"Experience"} id={"experience"} />
        <div className="flex flex-col ipad:flex-row">
          {/* This relative is to allow the position:absolute of the green left border */}
          {/*  TABS  */}
          <ol
            className={`
              mr-12 mb-8 ipad:mb-0 relative
              invisible-scroll overflow-x-scroll ipad:overflow-x-visible
              flex flex-row ipad:flex-col
              ${space_mono.className}
              text-sm
              `}
          >
            {/* This div is the green border that will shift depending on the tabIndex */}
            <div
              className={`
              bg-green
              absolute
              transition-[var(--transition)]
              bottom-0 left-0 ipad:top-0
              w-[130px] h-[2px] ipad:w-[2px] ipad:h-[40px]
              ${translateX[tabIndex]}
              ${translateY[tabIndex]} ipad:translate-x-0
              `}
            />
            {tabs.map((tabName: string, index: number) => (
              <li key={index}>
                <button
                  onClick={() => setTabIndex(index)}
                  tabIndex={index}
                  className={`
                  hover:green hover:bg-navy-800
                  focus:green
                  px-5
                  h-[40px] w-[130px] ipad:w-full
                  border-b-2 border-b-navy-700
                  ipad:border-l-2 ipad:border-l-navy-700 ipad:border-b-0
                  `}
                >
                  {tabName}
                </button>
              </li>
            ))}
          </ol>
          <div>{sections[tabIndex][1]()}</div>
        </div>
      </div>
    </FadeInViewOnceDiv>
  );
};

export default Experience;
