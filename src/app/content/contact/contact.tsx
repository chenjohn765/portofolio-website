import FadeInViewOnceDiv from "@/app/clientComponents/fadeInViewOnceDiv";
import { GreenButton, XXLTitle } from "@/app/components";
import { space_mono } from "@/app/fonts";
import Link from "next/link";

const Contact = () => (
  <FadeInViewOnceDiv>
    <div className="flex flex-col items-center h-screen">
      <p
        id="contact"
        className={`green text-[18px] mr-1 mb-4 ${space_mono.className}`}
      >
        {"04. What's next ?"}
      </p>
      <XXLTitle>Get In Touch</XXLTitle>
      <p className="mb-8">
        Come say hi, I will try my best to get back to you !
      </p>
      <Link target="_blank" href="mailto:chenJohn765@gmail.com">
        <GreenButton>Say Hi !</GreenButton>
      </Link>
    </div>
  </FadeInViewOnceDiv>
);
export default Contact;
