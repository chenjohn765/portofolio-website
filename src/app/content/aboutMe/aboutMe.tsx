import FadeInViewOnceDiv from "@/app/clientComponents/fadeInViewOnceDiv";
import { GreenImageWithBorder, SectionTitle } from "../../components";

const AboutMe = () => (
  <FadeInViewOnceDiv>
    <div className="mb-48">
      <SectionTitle index={"01"} title={"About Me"} id={"aboutme"} />
      <div className="flex gap-8 flex-col ipad:flex-row">
        <div className="mb-8">
          <ul className="grid grid-cols-2 gap-8">
            <div className="col-span-2 ipad:col-span-1">
              <li className="before-list-icon">Code stuff in web2/web3</li>
              <li className="before-list-icon">
                Hoyoslave (plays Honkai impact, Genshin impact, Honkai star
                rail)
              </li>
            </div>
            <div className="col-span-2 ipad:col-span-1">
              <li className="before-list-icon">
                Loses 90% of the time at chess
              </li>
              <li className="before-list-icon">
                Graduated from CentraleSupelec engineering school
              </li>
            </div>
          </ul>
          <br />
          <br />
          Techstack:
          <ul className="grid grid-cols-2 gap-8">
            {/* This before: will add an element before the actual html object */}
            <div className="col-span-2 ipad:col-span-1">
              <li className="before-list-icon">Python/FastAPI</li>
              <li className="before-list-icon">Terraform</li>
              <li className="before-list-icon">AWS services (Lambda, Glue)</li>
            </div>
            <div className="col-span-2 ipad:col-span-1">
              <li className="before-list-icon">NextJS/React</li>
              <li className="before-list-icon">
                A bit of Solidity/etherJS/eth-brownie (for ethereum smart
                contract)
              </li>
            </div>
          </ul>
        </div>
        {/* For PFP image */}
        <GreenImageWithBorder
          src="/pfp_me.jpg"
          height={832}
          width={640}
          alt={"loading ..."}
        />
      </div>
    </div>
  </FadeInViewOnceDiv>
);

export default AboutMe;
