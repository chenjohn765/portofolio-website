import { GreenImageWithBorder, SMTitle, SectionTitle } from "../../components";

import Link from "next/link";
import { BaseImageTemplate } from "./template";

const mintChillingWebsite = "https://mint-chilling-website.vercel.app/";

const MintChillingProjectReversed = () => (
  <BaseImageTemplate
    isLeftImage={false}
    imageChildren={
      <Link href={mintChillingWebsite} className="w-[60%]" target="_blank">
        <GreenImageWithBorder
          src={"/mint_chilling.png"}
          width={827}
          height={547}
          alt="loading ..."
        />
      </Link>
    }
    title={"MintChilling NFT"}
    description={
      <>
        A small NFT endeavour to learn some skills about NFTs. The contract is
        on the Sepolia test network. ERC721A, public/Whitelist mint using merkle
        proof, metadata on Pinata.
      </>
    }
    tags={["NFT", "ERC721A", "MerkleProof"]}
    websiteLink={mintChillingWebsite}
  />
);

export default MintChillingProjectReversed;
