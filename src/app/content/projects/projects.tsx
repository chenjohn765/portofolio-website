import { SectionTitle } from "../../components";
import FadeInViewOnceDiv from "../../clientComponents/fadeInViewOnceDiv";
import MintChillingProject from "./mintChilling";
import MintChillingProjectReversed from "./mintChillingReversed";

const Projects = () => {
  return (
    <FadeInViewOnceDiv>
      <div className="mb-48">
        <SectionTitle
          id={"projects"}
          index={"03"}
          title={"Some small projects"}
        />
        <ul>
          <li className="mb-24">
            <MintChillingProject />
          </li>
          <li className="mb-24">
            <MintChillingProjectReversed />
          </li>
        </ul>
      </div>
    </FadeInViewOnceDiv>
  );
};

export default Projects;
