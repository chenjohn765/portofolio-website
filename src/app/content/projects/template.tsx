import { SMTitle } from "@/app/components";
import { ExternalLinkLogo } from "@/app/svg";
import Link from "next/link";
import { ReactNode } from "react";

interface ITemplate {
  imageChildren: ReactNode;
  title: string;
  description: ReactNode;
  tags: string[];
  websiteLink: string;
}

export const BaseImageTemplate = ({
  imageChildren,
  title,
  description,
  tags,
  websiteLink,
  isLeftImage,
}: ITemplate & { isLeftImage: boolean }) => (
  <div className="grid grid-cols-12">
    {/* This row-start-1 will allows the two elements to overlap each other */}
    <div
      className={`
      row-start-1
      ${isLeftImage ? "col-start-1 col-end-7" : "col-start-6 col-end-[-1]"}
    `}
    >
      {imageChildren}
    </div>
    <div
      className={`
      row-start-1 z-[2]
      ${
        isLeftImage
          ? "col-start-6 col-end-[-1] text-right"
          : "col-start-1 col-end-7 text-left"
      }
      `}
    >
      {/* Div to put items at the right */}
      <div
        className={`flex flex-col ${isLeftImage ? "items-end" : "items-start"}`}
      >
        <p className="text-[13px] green">Featured Project</p>
        <SMTitle>{title}</SMTitle>
        <div className="mt-8 mb-8 rounded bg-navy-800 z-[2] p-6">
          {description}
        </div>
        <ul className="inline-flex mb-4 gap-4 text-[13px]">
          {tags.map((tagName: string, index: number) => (
            <li key={index}>{tagName}</li>
          ))}
        </ul>
        <div className="w-5 h-5">
          <Link href={websiteLink} target="_blank" className="hover:green">
            <ExternalLinkLogo />
          </Link>
        </div>
      </div>
    </div>
  </div>
);

// export const LeftImageTemplate2 = ({
//   imageChildren,
//   title,
//   description,
//   tags,
//   websiteLink,
// }: ITemplate) => (
//   <div className="grid grid-cols-12">
//     {/* This row-start-1 will allows the two elements to overlap each other */}
//     <div className="row-start-1 col-start-1 col-end-7">{imageChildren}</div>
//     <div className="row-start-1 col-start-6 col-end-[-1] z-[2] text-right">
//       {/* Div to put items at the right */}
//       <div className="flex flex-col items-end">
//         <p className="text-[13px] green">Featured Project</p>
//         <SMTitle>{title}</SMTitle>
//         <div className="mt-8 mb-8 rounded bg-navy-800 z-[2] p-6">
//           {description}
//         </div>
//         <ul className="inline-flex mb-4 text-[13px]">
//           {tags.map((tagName: string, index: number) => (
//             <li key={index} className="ml-4">
//               {tagName}
//             </li>
//           ))}
//         </ul>
//         <div className="w-5 h-5">
//           <Link href={websiteLink} target="_blank" className="hover:green">
//             <ExternalLinkLogo />
//           </Link>
//         </div>
//       </div>
//     </div>
//   </div>
// );

// export const RightImageTemplate2 = ({
//   imageChildren,
//   title,
//   description,
//   websiteLink,
//   tags,
// }: ITemplate) => (
//   <div className="grid grid-cols-12">
//     {/* This row-start-1 will allows the two elements to overlap each other */}
//     <div className="row-start-1 col-start-1 col-end-7 z-[2] text-left">
//       {/* Div to put items at the left */}
//       <div className="flex flex-col items-start">
//         <p className="text-[13px] green">Featured Project</p>
//         <SMTitle>{title}</SMTitle>
//         <div className="mt-8 mb-8 rounded bg-navy-800 z-[2] p-6">
//           {description}
//         </div>
//         <ul className="inline-flex mb-4 text-[13px]">
//           {tags.map((tagName: string, index: number) => (
//             <li key={index} className="mr-4">
//               {tagName}
//             </li>
//           ))}
//         </ul>
//         <div className="w-5 h-5">
//           <Link href={websiteLink} target="_blank" className="hover:green">
//             <ExternalLinkLogo />
//           </Link>
//         </div>
//       </div>
//     </div>
//     <div className="row-start-1 col-start-6 col-end-[-1]">{imageChildren}</div>
//   </div>
// );
