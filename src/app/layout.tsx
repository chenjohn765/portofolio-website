import { inter } from "./fonts";
import "./globals.css";

export const metadata = {
  title: "Johnny Chen",
  description: "Johnny Chen's website",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    // This scroll padding top property is to offset the fact that we have a sticky navbar
    // on top, whose size is exactly 24rem or 96px
    <html className="!scroll-smooth !scroll-pt-24" lang="en">
      <body className={inter.className}>{children}</body>
      {/* <body>
        <p className="font-spaceMono">
          Lorem ipsum dolor sit amet. Id eius quia aut amet voluptas et ducimus
          dolores qui nemo molestias ut corporis iusto qui porro quos et maxime
          error. Ea officia dicta eos vitae corrupti et quaerat tempore. Et
          aliquam quas ea autem distinctio vel veniam omnis ut obcaecati
          aperiam. Cum omnis voluptatum in culpa internos aut nihil animi qui
          suscipit exercitationem eum eveniet libero sit cumque autem. Qui
          dignissimos corrupti ut delectus nostrum ea eaque quia. Ea facere
          soluta ut sapiente voluptate aut repudiandae consectetur eos itaque
          sint. Rem nisi voluptatem sed corrupti quae ab aspernatur sequi!
        </p>
        <br />
        <br />
        <p className={space_mono.className}>
          Lorem ipsum dolor sit amet. Id eius quia aut amet voluptas et ducimus
          dolores qui nemo molestias ut corporis iusto qui porro quos et maxime
          error. Ea officia dicta eos vitae corrupti et quaerat tempore. Et
          aliquam quas ea autem distinctio vel veniam omnis ut obcaecati
          aperiam. Cum omnis voluptatum in culpa internos aut nihil animi qui
          suscipit exercitationem eum eveniet libero sit cumque autem. Qui
          dignissimos corrupti ut delectus nostrum ea eaque quia. Ea facere
          soluta ut sapiente voluptate aut repudiandae consectetur eos itaque
          sint. Rem nisi voluptatem sed corrupti quae ab aspernatur sequi!
        </p>
      </body> */}
    </html>
  );
}
