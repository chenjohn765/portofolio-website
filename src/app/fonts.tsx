import { Inter } from "next/font/google";
import { Space_Mono } from "next/font/google";

export const space_mono = Space_Mono({ weight: "400", subsets: ["latin"] });
export const inter = Inter({ subsets: ["latin"] });
