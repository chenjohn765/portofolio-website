import Link from "next/link";
import { GreenButton, MaxXLTitle } from "../components";
import { Inter } from "next/font/google";
import FadeInViewOnceDiv from "../clientComponents/fadeInViewOnceDiv";
import { space_mono } from "../fonts";

const inter = Inter({ subsets: ["latin"] });
const Intro = () => (
  <FadeInViewOnceDiv>
    <div className="mb-48">
      <p className={`green ${space_mono.className}`}>Hi, my name is</p>
      <MaxXLTitle>Johnny CHEN</MaxXLTitle>
      <h2
        className={`${inter.className} text-[#8892b0] text-[60px] font-bold pb-8 mt-[-20px]`}
      >
        Fullstack dev
      </h2>
      <p className="ipad:w-[60%] pb-8">
        I shamelessly copied this website from{" "}
        <Link
          className="green hover:underline"
          href="https://v4.brittanychiang.com/#"
        >
          {"Brittany Chiang's"}
        </Link>{" "}
        website because I suck at design. It was a training objective. Please
        have a look at her website and her other amazing projects.
        <br />
        <br />
        This website was built using NextJS & TailwindCSS.
      </p>
      <Link href="#">
        <GreenButton>Useless button</GreenButton>
      </Link>
    </div>
  </FadeInViewOnceDiv>
);
export default Intro;
