"use client";
import Link from "next/link";
import { JLogo } from "../svg";
import { SocialMedia } from "../sideBar/sideBar";
import { space_mono } from "../fonts";
import { HidePhone } from "../components";
import { useContext, useState } from "react";
import { zIndexes } from "../zIndex";
import { BlurContentContext } from "../contexts";

const Menu = ({ isOpen }: { isOpen: boolean }) => (
  <div
    className={`
    flex flex-col
  bg-navy-800 h-screen
    fixed top-0 right-0 w-[60%]
    pr-12 pt-10
    transition-[var(--transition)]
    shadow-[-10px_0_30px_-15px_var(--navy-shadow)]
    ${isOpen ? "translate-x-0" : "invisible translate-x-full"}
    `}
  >
    <ol
      className="
      flex flex-col items-center justify-center
      "
    >
      <li className="p-4">
        <Link className="hover:green" href="#aboutme">
          <span className="green">01.</span> About Me
        </Link>
      </li>
      <li className="p-4">
        <Link className="hover:green" href="#experience">
          <span className="green">02.</span> Experience
        </Link>
      </li>
      <li className="p-4">
        <Link className="hover:green" href="#projects">
          <span className="green">03.</span> Projects
        </Link>
      </li>
      <li className="p-4">
        <Link className="hover:green" href="#contact">
          <span className="green">04.</span> Contact
        </Link>
      </li>
    </ol>
  </div>
);

const HamburgerMenu = () => {
  const [isOpen, setIsOpen] = useState(false);
  const setShouldBlurContent = useContext(BlurContentContext);

  return (
    <>
      <Menu isOpen={isOpen} />
      {/* The relative position is to allow absolute positioning of children */}
      <button
        onClick={() => {
          setShouldBlurContent(!isOpen);
          setIsOpen(!isOpen);
        }}
        className={`
        w-[30px] h-[24px] ${zIndexes.hamburgerMenu} relative
        ${isOpen ? "ham-center-active" : "ham-center"}
        `}
      >
        <div
          className={`h-[2px] bg-green absolute right-0 ${
            isOpen
              ? "w-full top-1/2 opacity-0 ham-before-active"
              : "w-[120%] top-0 opacity-100 ham-before"
          }`}
        ></div>
        <div
          className={`h-[2px] bg-green absolute right-0 w-full top-1/2 ${
            isOpen ? "" : ""
          }`}
        ></div>
        <div
          className={`h-[2px] bg-green absolute right-0 ${
            isOpen
              ? "w-full top-1/2 ham-after-active"
              : "w-[80%] bottom-0 ham-after"
          }`}
        ></div>
      </button>
    </>
  );
};

const NavBar = () => (
  <nav
    className={`flex items-center w-full h-24 text-sm bg-navy-900 ${space_mono.className}`}
  >
    <a href="#" className="h-12 w-12 green mr-8">
      <JLogo />
    </a>
    <HidePhone>
      <SocialMedia />
    </HidePhone>
    <HidePhone className="flex ml-auto " alt={<HamburgerMenu />}>
      <ol className="flex ml-auto">
        <li className="p-4">
          <Link className="hover:green" href="#aboutme">
            <span className="green">01.</span> About Me
          </Link>
        </li>
        <li className="p-4">
          <Link className="hover:green" href="#experience">
            <span className="green">02.</span> Experience
          </Link>
        </li>
        <li className="p-4">
          <Link className="hover:green" href="#projects">
            <span className="green">03.</span> Projects
          </Link>
        </li>
        <li className="p-4">
          <Link className="hover:green" href="#contact">
            <span className="green">04.</span> Contact
          </Link>
        </li>
      </ol>
    </HidePhone>
  </nav>
);

export default NavBar;
