/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      screens: {
        // default value of sm
        ipad: "640px",
      },
      colors: {
        green: "#64ffda",
        navy: {
          900: "#0a192f",
          800: "#112240",
          700: "#233554",
        },
        gray: {
          900: "#8892b0",
          700: "#ccd6f6",
        },
      },
    },
  },
  plugins: [],
};
